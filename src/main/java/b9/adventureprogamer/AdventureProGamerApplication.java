package b9.adventureprogamer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdventureProGamerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdventureProGamerApplication.class, args);
    }

}

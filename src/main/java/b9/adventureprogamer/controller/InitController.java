package b9.adventureprogamer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InitController {

    @RequestMapping("/")
    public String index(){

        return "Hello, baby";

    }
}

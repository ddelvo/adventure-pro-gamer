# Adventure of Pro-gamer

[![pipeline status](https://gitlab.com/ddelvo/adventure-pro-gamer/badges/master/pipeline.svg)](https://gitlab.com/ddelvo/adventure-pro-gamer/-/commits/master) [![coverage report](https://gitlab.com/ddelvo/adventure-pro-gamer/badges/master/coverage.svg)](https://gitlab.com/ddelvo/adventure-pro-gamer/-/commits/master)

![main.PNG](image/main.PNG)

## Team

1. Ariasena Cahya Ramadhani (1906292925)
2. Vanessa Emily Agape (1906350793)
3. Nadya Aprillia (1906398566)
4. Naufal Sani (1906398723)
5. Muhammad Rizki Fauzan (1906400375)

## Ide Aplikasi 

Merupakan sebuah web based rpg game yang dimana User bermain sebagai Adventurer dan dapat memilih role Paladin, Berserker, atau Slayer untuk memulai petualangan. Kemudian user dapat merasakan berbagai event random mendapatkan reward sesuai action yang diambil. User juga bisa berbelanja di shop selama adventure dimana item tersebut akan dapat meningkatkan status dari User dan memudahkan petualangan. Suatu adventure diakhiri dengan final event melawan Boss yang cukup kuat. 

## Deployment

Main game
https://adventureprogamer.herokuapp.com/

## Game Flow

![view6.png](image/view6.png)



